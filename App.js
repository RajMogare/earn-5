import logo from './logo.svg';
import './App.css';
import * as yup from 'yup';
import React,{useState} from 'react';

function App() {

  const [firstName,setFirstName]=useState('');
  const [lastName, setLastName]=useState('');
  const [mobile,setMobile]=useState('');
  const [age, setAge]=useState('');
  const[ email,setEmail]=useState('');
  const [password,setPassword]=useState('');
  
  const useSchema=yup.object().shape({
    firstName:yup.string().required(),
    lastName:yup.string(),
    mobile:yup.string(),
    age:yup.string(),
    email:yup.string().email().required(),
    password:yup.string().min(8).required()

  })

  async function validateForm(){
    let dataObject={
      firstName:firstName,
      lastName:lastName,
      mobile:mobile,
      age:age,
      email:email,
      password:password
    }

    const isValid=await useSchema.isValid(dataObject)
    if(isValid){
      alert('Form is Valid')
    }
    else{
      alert('Form is Invalid')
    }
  };
  return (
    <div className="main">
      <center>
      <form>
        <input placeholder='Enter the FirstName ' onChange={(e)=>setFirstName(e.target.value)}></input>
        <input placeholder='Enter the LastName' onChange={(e)=>setLastName(e.target.value)}></input>
        <input placeholder='Enter the Mobile Number ' onChange={(e)=>setMobile(e.target.value)}></input>
        <input placeholder='Enter the Age' onChange={(e)=>setAge(e.target.value)}></input>
        <input placeholder='Enter the Email' onChange={(e)=>setEmail(e.target.value)}></input>
        <input placeholder='Enter the Password' onChange={(e)=>setPassword(e.target.value)}></input>
        <button type=' submit' onClick={()=>validateForm()}> Submit </button>

      </form>
      </center>
      
    </div>
  );
}

export default App;
